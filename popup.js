// (C) 2016 Bogdan Gureev

//------------------------------------------------------------------------------

function getScheduleButtons()
{
	return document.getElementsByClassName( "scheduleButton" );
}

//------------------------------------------------------------------------------

function getScheduleDropButtons()
{
	return document.getElementsByClassName( "scheduleDropButton" );
}

//------------------------------------------------------------------------------

function getScheduleButton( id )
{
	return document.getElementById( id );
}

//------------------------------------------------------------------------------

function getCustomFilter()
{
	return document.getElementsByClassName( "customFilter" )[ 0 ];
}

//------------------------------------------------------------------------------

function getCustomFilterOk()
{
	return document.getElementById( "customFilterOk" );
}

//------------------------------------------------------------------------------

function getCustomFilterReset()
{
	return document.getElementById( "customFilterReset" );
}

//------------------------------------------------------------------------------

function getCustomFilterSave()
{
	return document.getElementById( "customFilterSave" );
}

//------------------------------------------------------------------------------

function enableElement( element )
{
	element.disabled = false;
}

//------------------------------------------------------------------------------

function disableElement( element )
{
	element.disabled = true;
}

//------------------------------------------------------------------------------

function toggleAll( action )
{
	var buttons = getScheduleButtons();
	for ( var i = 0; i < buttons.length; ++i )
		action( buttons[ i ] );

	var dropButtons = getScheduleDropButtons();
	for ( var i = 0; i < dropButtons.length; ++i )
		action( dropButtons[ i ] );

	action( getCustomFilter() );
	action( getCustomFilterOk() );
	action( getCustomFilterReset() );
	action( getCustomFilterSave() );
}

//------------------------------------------------------------------------------

function disableAll()
{
	toggleAll( disableElement );
}

//------------------------------------------------------------------------------

function enableAll()
{
	toggleAll( enableElement );
}

//------------------------------------------------------------------------------

function getMainTable()
{
	return document.getElementById( "mainTable" );
}

//------------------------------------------------------------------------------

function getScheduleButtonsTable()
{
	return document.getElementById( "scheduleButtonsTable" );
}

//------------------------------------------------------------------------------

function generateTableRow( table, code )
{
	table.innerHTML += '<tr><td>' + code + '</td></tr>';
}

//------------------------------------------------------------------------------

function generateMainTableRow( code )
{
	generateTableRow( getMainTable(), code );
}

//------------------------------------------------------------------------------

function generateScheduleButtonsTableRow( code )
{
	generateTableRow( getScheduleButtonsTable(), code );
}

//------------------------------------------------------------------------------

function generateFilterTitle()
{
	generateMainTableRow(
		'<div class="filterTitle">Available filters:</div>'
	);
}

//------------------------------------------------------------------------------

function generateUnavailableFilter()
{
	generateMainTableRow(
		'<div class="unavailableFilter">No any filters are available on this site</div>'
	);
}

//------------------------------------------------------------------------------

function generateEmptyFilter()
{
	generateScheduleButtonsTableRow(
		'<div class="emptyFilter">No any filters were saved</div>'
	);
}

//------------------------------------------------------------------------------

function dropEmptyFilter()
{
	var emptyFilters = document.getElementsByClassName( "emptyFilter" );
	for ( var i = 0; i < emptyFilters.length; ++i )
	{
		// NOTE: div -> td -> tr -> tbody
		var row = emptyFilters[ i ].parentNode.parentNode.parentNode;
		var table = row.parentNode;
		table.removeChild( row );
	}
}

//------------------------------------------------------------------------------

function generateFilterButton( subjectName )
{
	var buttons = getScheduleButtons();
	for ( var i = 0; i < buttons.length; ++i )
		if ( buttons[ i ].value == subjectName )
			return;

	dropEmptyFilter();

	generateScheduleButtonsTableRow(
		'<table class="customFilterButtons" cellpadding="0" cellspacing="0">'
	+	'<tr>'
	+	'	<td><button class="scheduleButton" value="' + subjectName + '">' + subjectName + '</button></td>'
	+	'	<td><button class="scheduleDropButton" value="' + subjectName + '">X</button></td>'
	+	'</tr>'
	+	'</table>'
	);

	setupUserFilters();
}

//------------------------------------------------------------------------------

function generateCustomFilterTable()
{
	generateMainTableRow( '<div class="filterTitle">Custom filter:</div>' );
	generateMainTableRow( '<input class="customFilter"></input>' );
	generateMainTableRow(
		'<table class="customFilterButtons" cellpadding="0" cellspacing="0">'
	+	'<tr>'
	+	'	<td><button id="customFilterOk" class="customFilterButton">OK</button></td>'
	+	'	<td><button id="customFilterReset" class="customFilterButton">Reset</button></td>'
	+	'	<td><button id="customFilterSave" class="customFilterButton">Save</button></td>'
	+	'</tr>'
	+	'</table>'
	);

	setupCustomFilterActions();
}

//------------------------------------------------------------------------------

function generateScheduleButtonsTable()
{
	generateMainTableRow(
		'<table id="scheduleButtonsTable" class="scheduleButtonsTable" cellpadding="0" cellspacing="0">'
	+	'</table>'
	);

	generateEmptyFilter();

	restoreScheduleButtons();
}

//------------------------------------------------------------------------------

function restoreScheduleButtons()
{
	chrome.storage.sync.get(
			"scheduleButtons"
		,	function ( objects )
			{
				// NOTE: Check if something was stored
				if ( !( "scheduleButtons" in objects ) )
					return;

				objects = objects[ "scheduleButtons" ];
				for ( var i = 0; i < objects.length; ++i )
					generateFilterButton( objects[ i ] );
			}
	);
}

//------------------------------------------------------------------------------

function saveScheduleButtons()
{
	var saveResult = [];
	var buttons = getScheduleButtons();
	for ( var i = 0; i < buttons.length; ++i )
		saveResult.push( buttons[ i ].value );

	chrome.storage.sync.set(
			{ "scheduleButtons": saveResult }
		,	function ()
			{
			}
	);
}

//------------------------------------------------------------------------------

function validateUrl( url )
{
	return url.toLowerCase().startsWith( "http://cist.nure.ua/ias/app/tt" );
}

//------------------------------------------------------------------------------

function customFilterFunctor( input )
{
	if ( input.value == "" )
		return;

	disableAll();

	var script = "filterSchedule( '" + input.value + "' )";
	chrome.tabs.executeScript({ code: script });
}

//------------------------------------------------------------------------------

function setupCustomFilter( input, executor )
{
	executor.removeEventListener(
			"click"
		,	customFilterFunctor
	);
	executor.addEventListener(
			"click"
		,	function ()
			{
				customFilterFunctor( input );
			}
		,	false
	);
}

//------------------------------------------------------------------------------

function setupUserFilters( buttons )
{
	var scheduleTable = getScheduleButtonsTable();
	var scheduleFilterButtons = scheduleTable.getElementsByClassName( "scheduleButton" );
	var scheduleDropButtons = scheduleTable.getElementsByClassName( "scheduleDropButton" );

	setupButtonFilters( scheduleFilterButtons );
	setupButtonDropFilters( scheduleDropButtons );
}

//------------------------------------------------------------------------------

function setupButtonFilters( buttons )
{
	for ( var i = 0; i < buttons.length; ++i )
		setupButtonFilter( buttons[ i ] );
}

//------------------------------------------------------------------------------

function setupButtonFilter( button )
{
	setupCustomFilter( button, button );
}

//------------------------------------------------------------------------------

function setupButtonDropFilters( buttons )
{
	for ( var i = 0; i < buttons.length; ++i )
		setupButtonDropFilter( buttons[ i ] );
}

//------------------------------------------------------------------------------

function buttonDropFilterFunctor( button )
{
	// NOTE: button -> td -> tr -> tbody
	var row = button.parentNode.parentNode.parentNode;
	var table = row.parentNode;

	var topRow = table.parentNode.parentNode.parentNode;
	var topTable = topRow.parentNode;
	topTable.removeChild( topRow );

	if ( !topTable.children.length )
		generateEmptyFilter();

	saveScheduleButtons();

	// NOTE: Setup filters again
	setupUserFilters();
}

//------------------------------------------------------------------------------

function setupButtonDropFilter( button )
{
	button.removeEventListener(
			"click"
		,	buttonDropFilterFunctor
	);
	button.addEventListener(
			"click"
		,	function ()
			{
				buttonDropFilterFunctor( button )
			}
		,	false
	);
}

//------------------------------------------------------------------------------

function resetFilterFunctor()
{
	disableAll();

	chrome.tabs.reload();
}

//------------------------------------------------------------------------------

function setupResetFilter( button )
{
	button.removeEventListener(
			"click"
		,	resetFilterFunctor
	);
	button.addEventListener(
			"click"
		,	resetFilterFunctor
		,	false
	);
}

//------------------------------------------------------------------------------

function saveFilterFunctor()
{
	var subjectName = getCustomFilter().value;
	if ( subjectName == "" )
		return;

	generateFilterButton( subjectName );
	saveScheduleButtons();
}

//------------------------------------------------------------------------------

function setupSaveFilter( button )
{
	button.removeEventListener(
			"click"
		,	saveFilterFunctor
	);
	button.addEventListener(
			"click"
		,	saveFilterFunctor
		,	false
	);
}

//------------------------------------------------------------------------------

function setupCustomFilterActions()
{
	setupCustomFilter( getCustomFilter(), getCustomFilterOk() );
	setupResetFilter( getCustomFilterReset() );
	setupSaveFilter( getCustomFilterSave() );
}

//------------------------------------------------------------------------------

document.addEventListener(
		"DOMContentLoaded"
	,	function ()
		{
			chrome.tabs.query(
					{
							active: true
						,	currentWindow: true
					}
				,	function ( tabs )
					{
						// NOTE: Somehow several tabs were returned. Just do nothing
						if ( tabs.length != 1 )
							return;

						generateFilterTitle();

						if ( !validateUrl( tabs[ 0 ].url ) )
						{
							generateUnavailableFilter();
							return;
						}

						generateScheduleButtonsTable();
						generateCustomFilterTable();
					}
			);
		}
	,	false
);

//------------------------------------------------------------------------------

chrome.runtime.onMessage.addListener(
	function ( request, sender, sendResponse )
	{
		enableAll();
	}
);

//------------------------------------------------------------------------------
