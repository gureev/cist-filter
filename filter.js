// (C) 2016 Bogdan Gureev

//------------------------------------------------------------------------------

function dropBlock( _className )
{
	var blocks = document.getElementsByClassName( _className );
	for ( var i = 0; i < blocks.length; ++i )
	{
		blocks[ i ].innerHTML = "";
	}
}

//------------------------------------------------------------------------------

function getLinks()
{
	return document.getElementsByClassName( "linktt" );
}

//------------------------------------------------------------------------------

function resetSubject( _subject )
{
	_subject.setAttribute( "bgcolor", "#FFFFFF" );
	_subject.innerHTML = "";
}

//------------------------------------------------------------------------------

function matchSubject( _cell, _subjectName )
{
	return _cell.innerText.toLowerCase().search( _subjectName ) != -1;
}

//------------------------------------------------------------------------------

function isDateTitleColumn( _column )
{
	return _column.classList.contains( "date" ) || _column.classList.contains( "week" );
}

//------------------------------------------------------------------------------

function isGroupTitleColumn( _column )
{
	return _column.classList.contains( "left" ) || _column.classList.contains( "leftname" );
}

//------------------------------------------------------------------------------

function dropLine( _table, _line )
{
	// NOTE: Find the top-most row with pair number and group name
	var mainLine = _line;
	do
	{
		// NOTE: Column with rowspan is top-most column in the day for this group
		if ( mainLine.children[ 0 ].hasAttribute( "rowspan" ) )
			break;

		var previousLine = mainLine.previousElementSibling;
		if ( !previousLine.children.length )
			break;

		// NOTE: Looks like line is the first line without rowspan and previous line only with dates
		if ( isDateTitleColumn( previousLine.children[ 0 ] ) )
			break;

		mainLine = previousLine;
	}
	while ( true )

	var spanValue = parseInt( mainLine.children[ 0 ].getAttribute( "rowspan" ) );
	if ( spanValue > 1 )
	{
		// NOTE: There is several groups in one pair
		var newSpanValue = spanValue - 1;

		// NOTE: Decrease span value due to drop line
		mainLine.children[ 0 ].setAttribute( "rowspan", newSpanValue );
		mainLine.children[ 1 ].setAttribute( "rowspan", newSpanValue );

		// NOTE: We must move "rowspan" value to the next line if it is needed
		if ( _line == mainLine )
		{
			var nextLine = mainLine.nextElementSibling;
			// NOTE: If line has no "rowspan" attribute, this is pair for other group in the same day: move data
			if ( !nextLine.children[ 0 ].hasAttribute( "rowspan" ) )
			{
				// NOTE: In backward direction
				nextLine.insertBefore( mainLine.children[ 1 ], nextLine.children[ 0 ] );
				nextLine.insertBefore( mainLine.children[ 0 ], nextLine.children[ 0 ] );
			}
		}
	}

	// NOTE: Drop empty line and find the other one
	_table.removeChild( _line );
}

//------------------------------------------------------------------------------

function notifyFinish()
{
	// NOTE: Notify popup about filter's end
	chrome.runtime.sendMessage({ finish: 1 });
}

//------------------------------------------------------------------------------

function filterSchedule( _subjectName )
{
	if ( _subjectName == "" )
	{
		notifyFinish();
		return;
	}

	_subjectName = new RegExp( _subjectName.toLowerCase(), "i" );

	dropBlock( "footer" );
	//dropBlock( "bottomtt" ); // NOTE: Do not remove footer with generation's time

	// NOTE: Eliminate all subjects except "_subjectName"
	var links = getLinks();
	for ( var i = 0; i < links.length; )
	{
		var tableCell = links[ i ].parentNode;

		if ( !matchSubject( tableCell, _subjectName ) )
		{
			resetSubject( tableCell );
			continue;
		}

		// NOTE: Increment counter only if we not removed line
		++i;
	}

	// NOTE: Eliminate empty line from schedule
	var tables = document.getElementsByClassName( "MainTT" );
	if ( tables.length != 1 )
	{
		notifyFinish();
		return;
	}

	// NOTE: Schedule was found. table -> tbody
	var table = tables[ 0 ].children[ 0 ];
	var lines = table.children;
	for ( var i = 0; i < lines.length; )
	{
		var line = lines[ i ];
		var isEmptyLine = true;

		var columns = line.cells;
		for ( var j = 0; j < columns.length; ++j )
		{
			var column = columns[ j ];

			// NOTE: Do not process "title" rows
			if ( isDateTitleColumn( column ) )
			{
				isEmptyLine = false;
				break;
			}

			// NOTE: Skip pair number and group name
			if ( isGroupTitleColumn( column ) )
				continue;

			if ( matchSubject( column, _subjectName ) )
			{
				isEmptyLine = false;
				break;
			}
		}

		if ( isEmptyLine )
		{
			dropLine( table, line );
			continue;
		}

		// NOTE: Increment counter only if we not removed line
		++i;
	}

	// NOTE: Notify popup about filter's end
	notifyFinish();
}

//------------------------------------------------------------------------------

// NOTE: Enable all buttons by default
notifyFinish();

//------------------------------------------------------------------------------
